package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class userdao {
	 public User findByLoginInfo(String loginId, String password) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, loginId);
	            pStmt.setString(2, password);
	            ResultSet rs = pStmt.executeQuery();


	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {
	                return null;
	            }

	            // 必要なデータのみインスタンスのフィールドに追加
	            String loginIdData = rs.getString("login_id");
	            String nameData = rs.getString("name");
	            return new User(loginIdData, nameData);

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	    }


	    /**
	     * 全てのユーザ情報を取得する
	     * @return
	     */
	    public List<User> findAll() {
	        Connection conn = null;
	        List<User> userList = new ArrayList<User>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM user WHERE login_id !='admin' ";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                String password = rs.getString("password");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;

	    }



			public void registInfo(String loginId,String password,String username,String birthdate) {

				 Connection conn = null;
			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            String source = password;
				          //ハッシュ生成前にバイト配列に置き換える際のCharset
				          Charset charset = StandardCharsets.UTF_8;
				          //ハッシュアルゴリズム
				          String algorithm = "MD5";

				          //ハッシュ生成処理
				          byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				          DatatypeConverter.printHexBinary(bytes);
				          String result = DatatypeConverter.printHexBinary(bytes);


			            // SELECT文を準備
			            String sql = "INSERT INTO user(login_id,password,name,birth_date,create_date,update_date)VALUES (?,?,?,?,NOW(),NOW())";

			             // SELECTを実行し、結果表を取得
			            PreparedStatement pStmt = conn.prepareStatement(sql);
			            pStmt.setString(1, loginId);
			            pStmt.setString(2, result);
			            pStmt.setString(3, username);
			            pStmt.setString(4,birthdate);

			            pStmt.executeUpdate();

			        } catch (SQLException | NoSuchAlgorithmException e) {

			            e.printStackTrace();

			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();

			                }
			            }
			        }
			}

			 public User findById(String id) {
			        Connection conn = null;
			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            String sql = "SELECT * FROM user WHERE id = ? ";

			             // SELECTを実行し、結果表を取得
			            PreparedStatement pStmt = conn.prepareStatement(sql);
			            pStmt.setString(1, id);

			            ResultSet rs = pStmt.executeQuery();


			            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			            if (!rs.next()) {
			                return null;
			            }

			            // 必要なデータのみインスタンスのフィールドに追加
			            int id1=rs.getInt("id");
			            String loginIdData = rs.getString("login_id");
			            String nameData = rs.getString("name");
			            Date birthdate =rs.getDate("birth_date");
			            String Updatedate=rs.getString("update_date");
			            String createdate=rs.getString("create_date");
			            return new User(id1,loginIdData, nameData,birthdate,Updatedate,createdate);

			        } catch (SQLException e) {
			            e.printStackTrace();
			            return null;
			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();
			                    return null;
			                }
			            }
			        }
			    }



			 public void updateInfo(String id,String password,String username,String birthdate) {

				 Connection conn = null;
			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            String source = password;
				          //ハッシュ生成前にバイト配列に置き換える際のCharset
				          Charset charset = StandardCharsets.UTF_8;
				          //ハッシュアルゴリズム
				          String algorithm = "MD5";

				          //ハッシュ生成処理
				          byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				          DatatypeConverter.printHexBinary(bytes);
				          String result = DatatypeConverter.printHexBinary(bytes);


			            // SELECT文を準備
			            String sql = " UPDATE user SET password=?,name=?,birth_date=? WHERE id=? ";

			             // SELECTを実行し、結果表を取得
			            PreparedStatement pStmt = conn.prepareStatement(sql);

			            pStmt.setString(1, result);
			            pStmt.setString(2, username);
			            pStmt.setString(3,birthdate);
			            pStmt.setString(4,id);

			            pStmt.executeUpdate();

			        } catch (SQLException | NoSuchAlgorithmException e) {
			            e.printStackTrace();

			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();

			                }
			            }
			        }
			}

			 public void userdelete(String id) {

				 Connection conn = null;
			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            String sql = " DELETE FROM user WHERE id=?  ";

			             // SELECTを実行し、結果表を取得
			            PreparedStatement pStmt = conn.prepareStatement(sql);

			            pStmt.setString(1, id);

			            pStmt.executeUpdate();

			        } catch (SQLException e) {
			            e.printStackTrace();

			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();

			                }
			            }
			        }
			}

			 			 public List<User> searchUser(String loginId, String name,String birthdate,String birthdate2) {

				 	Connection conn = null;
			        List<User> userList = new ArrayList<User>();

			        try {
			            // データベースへ接続
			            conn = DBManager.getConnection();

			            // SELECT文を準備
			            // TODO: 未実装：管理者以外を取得するようSQLを変更する
			            String sql = "SELECT * FROM user WHERE login_id !='admin'";

			            if(!loginId.equals("")){
			            	sql+=" AND login_id='"+loginId+"'";

			            }

			            if(!name.equals("")){
			            	sql+=" AND name LIKE '%" + name + "%'";

			            }

			            if(!birthdate.equals("")){
			            	sql+=" AND birth_date >= '"+birthdate +"'";

			            }

			            if(!birthdate2.equals("")){
			            	sql+=" AND birth_date <='"+ birthdate2 +"'";

			            }

			             // SELECTを実行し、結果表を取得
			            Statement stmt = conn.createStatement();
			            ResultSet rs = stmt.executeQuery(sql);

			            // 結果表に格納されたレコードの内容を
			            // Userインスタンスに設定し、ArrayListインスタンスに追加
			            while (rs.next()) {
			                int id = rs.getInt("id");
			                String loginId1 = rs.getString("login_id");
			                String name1 = rs.getString("name");
			                Date birthDate = rs.getDate("birth_date");
			                String password = rs.getString("password");
			                String createDate = rs.getString("create_date");
			                String updateDate = rs.getString("update_date");
			                User user = new User(id, loginId1, name1, birthDate, password, createDate, updateDate);

			                userList.add(user);
			            }
			        } catch (SQLException e) {
			            e.printStackTrace();
			            return null;
			        } finally {
			            // データベース切断
			            if (conn != null) {
			                try {
			                    conn.close();
			                } catch (SQLException e) {
			                    e.printStackTrace();
			                    return null;
			                }
			            }
			        }
			        return userList;

			    }
			 			public User findByLoginId(String loginId) {
			 		        Connection conn = null;
			 		        try {
			 		            // データベースへ接続
			 		            conn = DBManager.getConnection();

			 		            // SELECT文を準備
			 		            String sql = "SELECT * FROM user WHERE login_id = ? ";

			 		             // SELECTを実行し、結果表を取得
			 		            PreparedStatement pStmt = conn.prepareStatement(sql);
			 		            pStmt.setString(1, loginId);

			 		            ResultSet rs = pStmt.executeQuery();


			 		            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			 		            if (!rs.next()) {
			 		                return null;
			 		            }

			 		            // 必要なデータのみインスタンスのフィールドに追加
			 		            String loginIdData = rs.getString("login_id");
			 		            return new User(loginIdData);

			 		        } catch (SQLException e) {
			 		            e.printStackTrace();
			 		            return null;
			 		        } finally {
			 		            // データベース切断
			 		            if (conn != null) {
			 		                try {
			 		                    conn.close();
			 		                } catch (SQLException e) {
			 		                    e.printStackTrace();
			 		                    return null;
			 		                }
			 		            }
			 		        }
			 		    }
}

