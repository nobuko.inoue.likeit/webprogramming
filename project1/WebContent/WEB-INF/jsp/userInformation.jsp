<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ一覧</title>
</head>
<body>
    <div align="center">
        <h2>ユーザ一覧</h2>
         </div>
           <h2>${userInfo.name} さん </h2>
          <form class="form-signin" action="search" method="post">
    <a class="btn btn-primary" href="logout" role="button">ログアウト</a>
        <br>
	<a href="UserRegist" role="button">新規登録</a>

   <p>
    ログインID
    <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" >
            </p>
            <p>
    ユーザー名
    <input type="text" name="username" id="inputUsername" class="form-control" placeholder="ユーザー名">
            </p>
            <p>
  <label for="start">生年月日:</label>

<input type="date" id="date_start" name="trip-start"

       min="1940-01-01" max="2020-12-31">
                ～
<input type="date" id="date_end" name="trip-end"

       min="1940-01-01" max="2020-12-31">
    </p>

        <div align="left">
        <input  type="submit" value="検索">
        </form>
        </div>
	<hr size="10">
        <table border="1">
 	<c:forEach var="user" items="${userList}">
           <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->

                     <td>
                        <a class="btn btn-primary" href="userdetail?id=${user.id}">詳細</a>
						<c:if test="${userInfo.loginId== 'admin'}" >
                       <a class="btn btn-danger" href ="delete?id=${user.id}">削除</a>
                       </c:if>
						<c:if test="${userInfo.loginId==user.loginId or userInfo.loginId== 'admin' }">
						  <a class="btn btn-success" href="update?id=${user.id}">更新</a>
                       </c:if>
                     </td>
                   </tr>


                 </c:forEach>

                 </table>
           </div>


</body>
</html>

