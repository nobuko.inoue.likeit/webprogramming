<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta= charaset="UTF-8">
        <title>ログイン画面</title>

    </head>
    <body>
        <div align="center">
         <h1>ログイン画面</h1>
         <div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
            </div>

   <form class="form-signin" action="login" method="post">
  <input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
 <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
       <p>
       </p>
       <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
	</form>
	</div>
    </body>
</html>