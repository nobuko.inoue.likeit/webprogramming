<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報詳細参照</title>
        <h2>${userInfo.name} さん </h2>
</head>
<body>
    <div align="right">
    <a class="btn btn-primary" href="#" role="button">ログアウト</a>
    </div>

    <h1>ユーザ情報詳細参照</h1>

    <p>
    ログインID	${user.loginId}
    </p>
    <p>
    ユーザ名 	${user.name}
    </p>
    <p>
    生年月日	${user.birthDate}
    </p>
    <p>
    登録日時	${user.createDate}
    </p>
    <p>
    更新日時	${user.updateDate}
    </p>

    <a class="btn btn-primary" href="Loginlist" role="button">戻る</a>

   </body>

</html>