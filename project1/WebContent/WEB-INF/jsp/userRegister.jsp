<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ新規登録</title>
</head>
<body>
    <div align="right">
        <div></div>
    <a class="btn btn-primary" href="#" role="button">ログアウト</a>
    </div>
     <div align="center">
         <h1> ユーザ新規登録</h1>
         </div>
    <br>
    <center>
    <p>
      <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
        <form  action="UserRegist" method="post">
            <label>ログインID</label>
        <input type="text" name="loginId" id="inputLoginId" class="form-control" >
    </p>
    <p>
        <label>パスワード</label>
        <input type="password" name="password" id="inputPassword" class="form-control" >
    </p>
    <p>
        <label>パスワード（確認）</label>
        <input type="password" name="password1" id="inputPassword" class="form-control" >
    </p>
    <p>
        <label>ユーザ名</label>
        <input type="text" name="username" id="inputUserName" class="form-control" >
    </p>
    <p>
        <label>生年月日</label>
        <input type="date" name="birthdate" id="inputBirthDate" class="form-control" >
    </p>
    </center>
	<center>
    <input type="submit" value="登録">
        </form>
	</center>
    <div align="left">
    <a class="btn btn-primary" href="Loginlist" role="button">戻る</a>
    </div>
</body>
</html>