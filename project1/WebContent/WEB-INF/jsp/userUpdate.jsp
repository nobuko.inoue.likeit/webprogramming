<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ情報更新</title>
</head>
<body>
    <div align="right">
    <a class="btn btn-primary" href="#" role="button">ログアウト</a>
    </div>
     <div align="center">
         <h1> ユーザ情報更新</h1>
         </div>
     <form class="form-signin" action="update" method="post">
	<input type="hidden" value="${user.id}" name="id" >
    <br>
    <center>
    <p>
    ログインID
    ${user.loginId}
    </p>
    <p>
    パスワード
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
    </p>
    <p>
    パスワード（確認）
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required>
    </p>
    <p>
    ユーザ名
    <input type="text" value="${user.name}" name="username" id="inputUsername" class="form-control" placeholder="ユーザー名" required autofocus>
    </p>
    <p>
    生年月日
 <input type="date" value="${user.birthDate}" name="birthdate" id="inputBirthDate" class="form-control" >
    </p>
    </center>
    <center>
         <input type="submit" value="更新">
    </center>

    <div align="left">
    <a class="btn btn-primary" href="Loginlist" role="button">戻る</a>
    </div>
        </body>
</html>