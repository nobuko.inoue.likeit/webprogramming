<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>ユーザ削除確認</title>
</head>
<body>
    <div align="right">
    <a class="btn btn-primary" href="#" role="button">ログアウト</a>
    </div>
     <div align="center">
         <h1> ユーザ削除確認</h1>
         </div>
    <br>
    <center>

         <form class="form-signin" action="delete" method="post">
         <p>
         <input type="hidden" value="${user.loginId}" name="loginid" >
		<input type="hidden" value="${user.id}" name="id" >
        <input type="hidden" value="${user.name}" name="username" >
        <input type="hidden" value="${user.birthDate}" name="birthdate" >
        ログインＩＤ：${user.loginId}
        <br>
        を本当に削除してよろしいでしょうか。
        </p>
        <br>

    <input type="button" onclick="location.href='Loginlist'"value="キャンセル" >
     <input type="submit" value="OK">
   </form>
    </center>

    <div align="left">
    <a class="btn btn-primary" href="Loginlist" role="button">戻る</a>
    </div>
</body>
</html>